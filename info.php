<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory       = 'foldergallery';
$module_name            = 'Foldergallery';
$module_function        = 'page';
$module_version         = '3.6.6';
$module_platform        = '7.x';
$module_author          = '<a href="https://cms-lab.com" target="_blank">CMS-LAB</a>';
$module_license         = '<a href="https://cms-lab.com/_documentation/foldergallery/license.php" class="info" target="_blank">GNU General Public License</a>';
$module_license_terms   = '<a href="https://cms-lab.com/_documentation/foldergallery/license.php" class="info" target="_blank">License terms</a>';
$module_description     = 'Create an Image Gallery with folders as categories.';
$module_home            = 'https://cms-lab.com/';
$module_guid            = 'c362eb43-878d-492f-906f-57a07da6d0f6';


/**
 *  IMPORTANT
 *  All variables and constants are set in class 'foldergallery.php'.
 *
 **/ 
