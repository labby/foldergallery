<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

require_once LEPTON_PATH.'/modules/foldergallery/functions.php';

/**
 * Scans one directory rekursive with options
 * @return array
 * @param string $rootDir
 * @param array $allowedExtensions[optional]
 * @param array $invisibleFileNames[optional
 * @param integer $modus[option]  0 = Files, 1 = Files/Folders, 2 = Folders
 * @param bool $rekursiv[option] default = true
 * @param array $allData[option]
 */

// LEPTON_handle::register(["file_list","directory_list"]);

function scanDirectories(
        $rootDir,
        $allowedExtensions = array(),
        $invisibleFileNames = array(),
        $modus = 1,
        $rekursiv = true,
        $allData = array()
    ){
	
	// 1.1 run through content of root directory
	$aAllFiles = scandir($rootDir);
    // 1.2 try to sort
	natsort($aAllFiles);
	// 1.3 force to re-index the list
	$dirContent = array_merge($aAllFiles, []);

	foreach ($dirContent as $content) {
		// filter all files not accessible
		$path = $rootDir.'/'.$content;
		if (!in_array($content, $invisibleFileNames)) {
			// if content is file & readable, add to array
			if (is_file($path) && is_readable($path)) {
				$content_chunks = explode(".", $content);
				$ext = $content_chunks[count($content_chunks)-1];
				$ext = strtolower($ext);
				// only include files with desired extensions
				if ( (in_array($ext, $allowedExtensions)) && ($modus < 2) )
				{
				    $allData[] = $path;
				}
				// if content is a directory and readable, add path and name
			}
			elseif (is_dir($path) && is_readable($path)) {
				if ($modus > 0) {
					$allData[] = $path;
				}
				// recursive callback to open new directory
				if ($rekursiv) {
					$allData = scanDirectories($path, $allowedExtensions, $invisibleFileNames, $modus, $rekursiv, $allData);
				}
			}
		}
	}
	return $allData;
}

/**
 *
 * @return
 * @param string $rootDir
 * @param array $allowedExtensions
 * @param array $invisibleFileNames
 * @param integer $modus[optional]
 * @param bool $rekursiv[optional]
 */
function getFolderData($rootDir, $allowedExtensions, $invisibleFileNames, $modus = 1, $rekursiv = true)
{
	$daten = scanDirectories($rootDir, $allowedExtensions, $invisibleFileNames, $modus, $rekursiv);
	foreach ($daten as & $value) {
		$value = str_replace($rootDir, '', $value);
	}
	return $daten;
}

/**
 * Löscht das angegeben Verzeichnis und alle darin enthaltenen Unterverzeichnisse,
 * sowie die darin enthaltenen Files
 * @return int Fehlernummer
 * @param string $path Pfad zum Ornder
 */
function deleteFolder($path) {
	// schau' nach, ob das ueberhaupt ein Verzeichnis ist
	if (!is_dir($path)) {
		return -1;
	}
	// oeffne das Verzeichnis
	$dir = opendir($path);

	// Fehler?
	if (!$dir) {
		return -2;
	}

	// gehe durch das Verzeichnis
	while (($entry = @readdir($dir)) !== false) {
		// wenn der Eintrag das aktuelle Verzeichnis oder das Elternverzeichnis
		// ist, ignoriere es
		if ($entry == '.' || $entry == '..')
		{
			continue ;
		}
		// wenn der Eintrag ein Verzeichnis ist, dann
        if (is_dir($path.'/'.$entry)) {
			// rufe mich selbst auf
			// $res = deleteFolder($path.'/'.$entry);
            // wenn ein Fehler aufgetreten ist
            if ($res == -1) { // dies duerfte gar nicht passieren
                closedir($dir); // Verzeichnis schliessen
                return -2; // normalen Fehler melden
            }
            else if ($res == -2) { // Fehler?
                closedir($dir); // Verzeichnis schliessen
                return -2; // Fehler weitergeben
            }
            else if ($res == -3) { // nicht unterstuetzer Dateityp?
                closedir($dir); // Verzeichnis schliessen
                return -3; // Fehler weitergeben
            }
            else if ($res != 0) { // das duerfe auch nicht passieren...
                closedir($dir); // Verzeichnis schliessen
                return -2; // Fehler zurueck
            }
        }
        else if (is_file($path.'/'.$entry) || is_link($path.'/'.$entry)) {
	        // ansonsten loesche diese Datei / diesen Link
	        $res = unlink($path.'/'.$entry);
            // Fehler?
            if (!$res) {
	            closedir($dir); // Verzeichnis schliessen
                return -2; // melde ihn
            }
        }
        else {
	        // ein nicht unterstuetzer Dateityp
	        closedir($dir); // Verzeichnis schliessen
            return -3; // tut mir schrecklich leid...
        }
    } // end while
    // schliesse nun das Verzeichnis
    closedir($dir);
    // versuche nun, das Verzeichnis zu loeschen
    $res = rmdir($path);
    // gab's einen Fehler?
    if (!$res) {
	    return -2; // melde ihn
    }
    // alles ok
    return 0;
}


/**
 * Syncronisiert eine gesamte Bildergalerie, löscht alte Einträge oder erstellt neu in der DB
 * Dabei wird das Dateisystem als Grundlage genommen. 
 * @return bool true = sucsess
 * @param array	$galerie  Einstellungen dieser Galerie
 * @param string $searchCategorie ab welchem ordner gescannt werden soll, relativ zum Stammordner
 * @param integer $modus [optional] 0,1,2 Modus
 * @param bool $rekursiv [optional] soll rekursiv gesucht werden
 *
 * @see scanDirectories()
 * @see deleteFolder()
 */
function syncDB($galerie, $searchCategorie = '') {

	// Auf diese Variablen muss zugegriffen werden
	global $database;
	global $invisibleFileNames;
	global $url;
	global $path;
	global $thumbdir;
	
	
	// Daten Vorbereiten
	$rootDir = foldergallery::FG_PATH.$galerie['root_dir'];
	$searchFolder = $rootDir.$searchCategorie;
	$extensions = explode(',', $galerie['extensions']);
	
	$invisible = array_merge( foldergallery::INVISIBLE_FILE_NAMES, explode(',', $galerie['invisible']));

	//Alle Angaben aus dem Filesystem holen
	$allData = getFolderData($searchFolder, $extensions, $invisible);
	//natsort($allData); # ! Bringt es das?
	//Angaben auswerten
	$categories = array ();
	$files = array ();
	foreach ($allData as $data) {
		$einzelteile = explode('/', $data);
		$letztesElement = count($einzelteile)-1;
		if (substr_count($einzelteile[$letztesElement], '.') == 0) {
			//Hier werden alle Kategorien angelegt
			$catName = $einzelteile[$letztesElement];
			unset ($einzelteile[$letztesElement]);
			$catParents = implode('/', $einzelteile);
			$catParents = $searchCategorie.$catParents;
			$categories[] = array (
				'categorie'=>$catName,
				'parent'=>$catParents,
				'is_empty'=>1
			);
		} else {
			//Hier gehts um die Files
			$fileName = $einzelteile[$letztesElement];
			
			unset ($einzelteile[$letztesElement]);
			$parent = implode('/', $einzelteile);
			$parent = $searchCategorie.$parent;			
			$fileLink = foldergallery::FG_URL.$galerie['root_dir'].$parent."/".$fileName; 
			$fileLink = str_replace(LEPTON_URL, '', $fileLink);
		
			$files[] = array (
				'file_name'=>$fileName,
				'file_link'=>$fileLink,				
				'parent'=>$parent
			);
		}
	}
	// Kategorien mit Bildern finden
	foreach ($categories as &$nameCat) {
		$catString = $nameCat['parent']."/".$nameCat['categorie'];
		foreach ($files as $file) {
			if ($file['parent'] == $catString)
			{
				$nameCat['is_empty'] = 0;
				break;
			}
		}
	}
    unset($nameCat);
    
	// Falls Parents, diese finden
	foreach ($categories as &$nameCat) {
		$catName = $nameCat['categorie'];
		foreach ($categories as $searchCat) {
	    	if ((strpos($searchCat['parent'], $catName) !== false) && (!$searchCat['is_empty']))
	    	{
				$nameCat['is_empty'] = 0;
				break;
			}
		}
	}
	unset($nameCat);
	
	// Kategorien mit DB synchronisieren
	// Neuer SQL vorbereiten
	$notDeleteArray = array();
// a.3	
	$insertSQL = "
	INSERT INTO `".TABLE_PREFIX."mod_foldergallery_categories`
	    (`section_id`, `categorie`, `parent`, `cat_name`, `is_empty`, `childs`, `description`)
	VALUES ";
	
	
	$deleteSQL = "DELETE FROM ".TABLE_PREFIX."mod_foldergallery_categories WHERE parent_id > '0' AND section_id=".$galerie['section_id'] ; 
	$deleteLaenge = strlen($deleteSQL);
	$insertLaenge = strlen($insertSQL);
	foreach ($categories as $cat) {
// a.2
        $aTempResult3 = [];
		$sql = "
		    SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories`
			WHERE `section_id`=".$galerie['section_id']."
			 AND  `categorie`='".$cat['categorie']."'
			 AND  `parent`='".$cat['parent']."'
			LIMIT 1;";
			
		$database->execute_query(
		    $sql,
		    true,
		    $aTempResult3,
		    false
		);
		
		if(count($aTempResult3) > 0)
		{
		    $result = $aTempResult3; 
			if($result['is_empty'] == $cat['is_empty']){
				$notDeleteArray[] = $result['id'];
			} else {
				// Falls die Kategorie schon existierte nehmen wir für die neuen Einträge diejenigen von der DB
				$insertSQL .= " (".$result['section_id'].", '".$result['categorie']."', '".$result['parent']."', '".$result['cat_name']."', ".$cat['is_empty'].", '', ''),";
				// Diese Datensätze müssen aber zuerst gelöscht werden, da sie sonst doppelt vorkommen würden!
			}
		} else {
			// Sonst erstellen wir einfach einen neuen Standarddatensatz
			$cat_name = str_replace('_', ' ',$cat['categorie']);
			$cat_name = str_replace('-', ' ',$cat_name);
			$insertSQL .= " (".$galerie['section_id'].", '".$cat['categorie']."', '".$cat['parent']."', '".$cat_name."', ".$cat['is_empty'].", '', ''),";
		}
	}
	// SQL zum löschen der alten Einträge
	if(!empty($notDeleteArray)) {
		$deleteSQL .= ' AND (id NOT IN( '.implode(',',$notDeleteArray).'))';
	}
	if($searchCategorie != ''){
		$deleteSQL.= ' AND (parent REGEXP("'.$searchCategorie.'"))';
	}
	if(strlen($deleteSQL) != $deleteLaenge){
		$deleteSQL .= ';';	
		$database->query($deleteSQL);
	}
	
	if(strlen($insertSQL) != $insertLaenge){
		// Jetzt fügen wir die neuen Einträge hinzu
		$insertSQL = substr($insertSQL, 0, -1).";";
		$database->query($insertSQL);
	}
	
	// So, dass waren die Kategorien, nun sind die Bilder an der Reihe
	
	//Die Felder "file_link" und "thumb_link" sind obsolet
	//Jetzt noch die Parents zu Ziffern umwandeln:
	
	//Wieder aus der Datenbank laden:	
	$catpathArray = array();
// a.1
    $aTempResult = [];	
    $sql = 'SELECT id, categorie, parent FROM '.TABLE_PREFIX.'mod_foldergallery_categories WHERE section_id='.$galerie['section_id'];
    $database->execute_query(
        $sql,
        true,
        $aTempResult,
        true
    );
    foreach($aTempResult as $result )
    {
        $p = $result['parent'].'/'.$result['categorie'] ;
        if ( $result['parent'] == -1)
        {
            $p = ""; // x.1
        }
        $catpathArray[$p] =  $result['id'];
    }

    $notDeleteArray = array();
    $insertSQL = "INSERT INTO ".TABLE_PREFIX."mod_foldergallery_files (file_name, parent_id, caption) VALUES";

    $laenge = strlen($insertSQL);
    $count = 0;
    foreach($files as $file)
    {

        if (!isset($catpathArray[$file['parent']]))
        {
            $parent_id = 0;
        }
        else
        {
            $parent_id = $catpathArray[$file['parent']];
        }
	
	    $aTempResult2 = [];
		
		$database->execute_query(
		    'SELECT * FROM `'.TABLE_PREFIX.'mod_foldergallery_files` 
		        WHERE `parent_id` ="'.$parent_id.'" 
		        AND `file_name`="'.$file['file_name'].'" 
		        LIMIT 1;',
		    true,
		    $aTempResult2,
		    false
		);
		
		if( empty($aTempResult2)) // Achtung
		{ 
			$count ++;
			$insertSQL .= " ('".$file['file_name']."', '".$parent_id."', ''),";
		}
	}
	
	// SQL für neue Einträge	
	$insertSQL = substr($insertSQL, 0, -1).";";
	
	if($laenge != strlen($insertSQL))
	{
		echo "added ".$count." files";
		$database->query($insertSQL);
	}	
	
	delete_files_with_no_cat();
	
	return true;
}

/**
 *  (see above) Delete all file(-entries) with no valid used cat id
 */
function delete_files_with_no_cat() {
	
	$database = LEPTON_database::getInstance();
	
	$aTempResult = [];
	
	$database->execute_query(
	    'SELECT `id` FROM `'.TABLE_PREFIX.'mod_foldergallery_categories`',
	    true,
	    $aTempResult,
	    true
	);
	$notDeleteArray = array();
	foreach( $aTempResult as $result)
	{		
		$notDeleteArray[] = $result['id'];
    }
	if( !empty($notDeleteArray) )
	{
		$deleteSQL = "DELETE FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE (`parent_id` NOT IN";
		$deleteSQL .= '(0,'.implode(',',$notDeleteArray).'));';
		$database->simple_query($deleteSQL);		
	}
}

function rek_db_delete($cat_id)
{

    $database = LEPTON_database::getInstance();

    $aTempResult = [];
    $database->execute_query(
        'SELECT `section_id`, `categorie`, `parent`, `has_child` FROM `'.TABLE_PREFIX.'mod_foldergallery_categories` WHERE `id`='.$cat_id.';',
        true,
        $aTempResult,
        false
    );

    if (!empty($aTempResult))
    {
        $database->simple_query('DELETE FROM `'.TABLE_PREFIX.'mod_foldergallery_files` WHERE `parent_id`="'.$cat_id.'";');

        if ($aTempResult['has_child'])
        {
            $aTempResult2 = [];
            $database->execute_query(
                'SELECT id FROM `'.TABLE_PREFIX.'mod_foldergallery_categories` WHERE `parent_id`='.$cat_id.';',
                true,
                $aTempResult2,
                true
            );

            foreach( $aTempResult2 as $select_result )
            {
                rek_db_delete($select_result['id']);
            }
        }
    }

    $database->simple_query('DELETE FROM `'.TABLE_PREFIX.'mod_foldergallery_categories` WHERE `id`='.$cat_id);
}
