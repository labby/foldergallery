<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// Delete DB-Entries (messages and settings)
$temp_parent_ids = array();
$database->execute_query(
	"SELECT `id` FROM ".TABLE_PREFIX."mod_foldergallery_categories WHERE section_id=". $section_id ,
	true,
	$temp_parent_ids,
	true
);

$sTempQueryStart = 'DELETE FROM `'.TABLE_PREFIX.'mod_foldergallery_';

foreach($temp_parent_ids as $parent) {
	$database->simple_query($sTempQueryStart.'files` WHERE `parent_id`='.$parent['id']);
}

$database->simple_query($sTempQueryStart."settings` WHERE `page_id` = ".$page_id." AND `section_id` = ".$section_id);
$database->simple_query($sTempQueryStart."categories` WHERE `section_id` = ".$section_id);

