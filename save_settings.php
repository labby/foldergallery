<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$page_id = LEPTON_core::getGlobal('page_id');
$section_id = LEPTON_core::getGlobal('section_id');
$TEXT = LEPTON_core::getGlobal("TEXT");

$database = LEPTON_database::getInstance();
$oFG = foldergallery::getInstance();
$oFG->init_section($page_id,$section_id);
$oldSettings = $oFG->fg_settings;

LEPTON_handle::include_files ('/modules/foldergallery/backend.functions.php');

$newSettings = [];

//	Get data from $_POST -- [1.2]
$newSettings['root_dir'] = filter_input( INPUT_POST, "root_dir", FILTER_SANITIZE_SPECIAL_CHARS ) ?? "";

if (isset($_POST['extensions']) && ($_POST['extensions'] != '')) {
    $extensions = strtolower($_POST['extensions']);
	$extensionsarray = explode(',',str_replace(' ', '', $extensions));
	$extensionsarray = array_unique($extensionsarray);
	$newSettings['extensions'] = implode(',', $extensionsarray);
} else {
    $newSettings['extensions'] = $oldSettings['extensions'];
}
if (isset($_POST['invisible'])) {
	$newSettings['invisible'] = $_POST['invisible'];
} else {
	$newSettings['invisible'] = $oldSettings['invisible'];
}
if (isset($_POST['pics_pp']) && is_numeric($_POST['pics_pp']) ) {
	$newSettings['pics_pp'] = $_POST['pics_pp'];
} else {
	$newSettings['pics_pp'] = $oldSettings['pics_pp'];
}

if (isset($_POST['thumb_size']) && is_numeric($_POST['thumb_size']) ) {
	$newSettings['thumb_size'] = (int) trim($_POST['thumb_size']);
} else {
	$newSettings['thumb_size'] = '';
}

if (isset($_POST['catpic']) && is_numeric($_POST['catpic']) ) {
	$newSettings['catpic'] = (int) $_POST['catpic'];
} else {
	$newSettings['catpic'] = $oldSettings['catpic'];
}

if (isset($_POST['ratio'])) {
	$newSettings['ratio'] = $_POST['ratio'];
} else {
	$newSettings['ratio'] = '';
}

if (isset($_POST['lightbox']) && file_exists( dirname(__FILE__).'/templates/frontend/view_'.$_POST['lightbox'].'.lte' ) ) {
	$newSettings['lightbox'] = $_POST['lightbox'];
} else {
	$newSettings['lightbox'] = '';
}

echo LEPTON_tools::display($oFG->language['SAVE_SETTINGS'],'pre','ui message');
$newSettings['section_id'] = $_POST['section_id'];

$settingsTable = TABLE_PREFIX.'mod_foldergallery_settings';

// save values in db
$fields = array(
    'root_dir'      => $newSettings['root_dir'], 
    'extensions'    => $newSettings['extensions'], 
    'invisible'     => $newSettings['invisible'],
    'pics_pp'       => $newSettings['pics_pp'],
    'thumb_size'    => $newSettings['thumb_size'],
    'ratio'         => $newSettings['ratio'],
    'catpic'        => $newSettings['catpic'],
    'lightbox'      => $newSettings['lightbox']
);

$database->build_and_execute(
	'update',
	$settingsTable,
	$fields,
	"`section_id` = '".$_POST['section_id']."'"
);

// delete thumbs if thumb size or ratio has changed
if(($oldSettings['thumb_size'] != $newSettings['thumb_size'] || $oldSettings['ratio'] != $newSettings['ratio']) && !isset($_POST['noNew'])){
	$all_data = [];
	$database->execute_query(
	    'SELECT `parent`, `categorie` FROM `'.TABLE_PREFIX.'mod_foldergallery_categories` WHERE `section_id`='.$oldSettings['section_id'].';',
	    true,
	    $all_data
	);
	
	foreach($all_data as $link) {
		$pathToFolder = foldergallery::FG_PATH.$oldSettings['root_dir'].$link['parent'].'/'.$link['categorie'].foldergallery::FG_THUMBDIR;
		echo LEPTON_tools::display('Delete: '.$pathToFolder,'pre','ui message');
		deleteFolder($pathToFolder);
	}
	
	$pathToFolder = foldergallery::FG_PATH.$oldSettings['root_dir'].foldergallery::FG_THUMBDIR;
	echo LEPTON_tools::display('Delete: '.$pathToFolder,'pre','ui message');
	deleteFolder($pathToFolder);
}	


// delete db entries
if($oldSettings['root_dir'] != $newSettings['root_dir']){
	$aEntriesToDelete = [];
	$database->execute_query(
	    "SELECT `parent`, `categorie` FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `section_id`=".$oldSettings['section_id'].";",
	    true,
	    $aEntriesToDelete,
	    true
	);

	foreach( $aEntriesToDelete as $cat)
	{
		if( true === isset( $cat['parent'] ) && ( 0 < intval( $cat['parent'] )) )
		{
		    $database->simple_query("DELETE FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `parent_id`=".$cat['parent']);
	    }
	}
	
	$database->simple_query( "DELETE FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `section_id`=".$oldSettings['section_id'].";" );
  
  // ?? Why this and why double parent ??
    $fields = array(
        "section_id"    => $_POST['section_id'],
        "parent_id"     => -1,
        "categorie"     => "Root",
        "cat_name"      => "Root",
        "active"        => 1,
        "is_empty"      => 0,
        "position"      => 0,
        "niveau"        => -1, // ?
        "has_child"     => 0,
        "childs"        => "",
        "description"   => "",
        "parent"        => "-1" // !
    );
    
    $database->build_and_execute(
        "insert",
        TABLE_PREFIX."mod_foldergallery_categories",
        $fields
    );
}

// [10] sync db
syncDB($newSettings);
echo(LEPTON_tools::display($section_id, 'pre','ui green message'));

// [11] Check/Generate thumbnails here!
if( false === foldergallery_files::getInstance()->checkThumbnails( $section_id ) )
{
    die("Huston - we've got a problem!");
}

// check if database is error
LEPTON_admin::getInstance()->print_success(
	$TEXT['SUCCESS'],
	LEPTON_URL.'/modules/foldergallery/sync.php?page_id='.$page_id.'&section_id='.$section_id
);

