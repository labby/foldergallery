<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$page_id    = LEPTON_core::getValue("page_id",    "integer", "get");
$section_id = LEPTON_core::getValue("section_id", "integer", "get");
$cat_id     = LEPTON_core::getValue("cat_id",     "integer", "get");

$oFG = foldergallery::getInstance();
$oFG->init_section($page_id, $section_id);

LEPTON_handle::include_files ('/modules/foldergallery/backend.functions.php');

if (!is_null($cat_id))
{
	$result = [];	
	LEPTON_database::getInstance()->execute_query(
        "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `id`= ".$cat_id,
        true,
        $result,
        false
    );		
	
	if (!empty($result))
	{	
		// delete files
		$delete_path = foldergallery::FG_PATH.$oFG->fg_settings['root_dir'].$result['parent'].'/'.$result['categorie'];
	
		// delete db entries
		rek_db_delete($cat_id);
		
		// delete image files
		LEPTON_handle::delete_obsolete_directories(str_replace(LEPTON_PATH, '', $delete_path));
		
		LEPTON_admin::getInstance()->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
	} 
	else 
	{
		LEPTON_admin::getInstance()->print_error($oFG->language['ERROR_MESSAGE'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
	}

}
LEPTON_admin::getInstance()->admin->print_footer();
