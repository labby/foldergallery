/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

/* define icons class names*/
var folder_open     = "large green folder open icon";
var folder_closed   = "large green folder icon ";
	
/**
 *  New by aldus
 *
 */
function fg_toggle_categorie( aRef, aChildIDList )
{ 
    var state = "table-row";
    
    var current_class = aRef.getAttribute("class");
    
    if(current_class == folder_closed) {
        aRef.setAttribute("class", folder_open);
    } else {
        aRef.setAttribute("class", folder_closed);
        state = "none";
    }
    
    var list = aChildIDList.split(",");

    for( let aldus1 of list )
    {
        document.getElementById("cat_item_"+aldus1).style.display = state;
        if(state == "none") 
        {
            hideChilds( aldus1 );
        }
    }

}

function hideChilds( aID ) {

    var ref = document.getElementById("sub_item_"+aID);

    if(ref)
    {
        var test = ref.getAttribute("onclick");
        var reg = /\'.*\'/;
        
        var s = reg.exec(test);
        s = s[0].replace(/\'/g, "");
        var s2 = s.split(",");

        for( let aldus2 of s2 )
        {
            document.getElementById("cat_item_"+aldus2 ).style.display = "none";
            hideChilds( aldus2 ); // and the next childs ...
        }

        ref.setAttribute("class", folder_closed);
    }
}

function fg_modifyThumb( aRef, aID, aLink) {
    var form = document.getElementById("fg_list_thumbnails");
    
    form.action = aLink;

    var ref = document.getElementById("my_edit_id");
    ref.value = aID;
    
    form.submit();
}