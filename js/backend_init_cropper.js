/**
 *  Initialize cropper script
 *
 *  @see: https://jamesooi.design/Croppr.js/
 *
 */

var croppr = new Croppr('#croppr', {
    // options
    aspectRatio : FOLDERGAL_PREVIEW_RATIO, // 1:1, 4:3, 16:9
    startSize: [ 80, 80 ],
    onCropMove: function(value) {
        myPrivateCallBack( value.x, value.y, value.width, value.height );
    },
    onCropEnd: function(value) {
        myPrivateCallBack( value.x, value.y, value.width, value.height );
    },
    onInitialize: function( myInstance )
    {
        
        if( 1 === FOLDERGAL_CUSTOM )
        {
            myInstance.resizeTo( FOLDERGAL_CUSTOM_W, FOLDERGAL_CUSTOM_H, [0,0] );
            myInstance.moveTo( FOLDERGAL_CUSTOM_X, FOLDERGAL_CUSTOM_Y );
        }
        
        var val = myInstance.getValue();
        myPrivateCallBack( val.x, val.y, val.width, val.height );
        
    }
});

function myPrivateCallBack( sourceX, sourceY, sourceW, sourceH ) {
    var c = document.getElementById("aldusCanvas");
    var ctx = c.getContext("2d");
    
    var img = document.getElementById("preview");

    ctx.drawImage(img, sourceX, sourceY, sourceW, sourceH, 0, 0, FOLDERGAL_PREVIEW_WIDTH, FOLDERGAL_PREVIEW_HEIGHT);
}

