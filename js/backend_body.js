/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
$(document).ready(function(){ 
	$(function() { 
		$("#fg_cat_table tr").sortable(
		    {
		        opacity: 0.6,
		        cursor: 'move',
		        connectWith: "tr",
		        update: function( event, ui) { 
                    console.log("res: "+$(this).sortable( "toArray" )); 
                } 
		    }
		); 
	}); 
 }); 
$("#fg_cat_table tr").disableSelection();

// Remember to invoke within jQuery(window).load(...)
// If you don't, Jcrop may not initialize properly

var aTemp = window.location.pathname.split("/");
var sFilename = aTemp[ aTemp.length -1 ];
console.log( sFilename );

if(sFilename == "modify_thumb.php") {
    
    $(window).load(function(){
        // here initialize
    });
}

function showPreview(coords)
{
    // old function call?
}


function updateCoords(c)
{
    // old function call
}

function checkCoords()
{
    var temp = croppr.getValue();
    
    $('#fgThumbX').val(temp.x);
    $('#fgThumbY').val(temp.y);
    $('#fgThumbW').val(temp.width);
    $('#fgThumbH').val(temp.height);
    
    return true;
}

function fg_toggleVisibileState( aRefID )
{
    var ref_name = "#cat_visi_"+aRefID;
    var class_active = "large green eye icon";
    var class_inactive = "large red eye slash icon";
    
    var state = -1;
    var x = $(ref_name);
    if( x.prop("class") === class_active)
    {
        x.prop("class", class_inactive);
        state = 0;
    } else {
        x.prop("class", class_active);
        state = 1;
    }
    
    if ( (typeof FOLDERGALLERY_URL !== 'undefined') && (typeof LEPTOKEN !== 'undefined') ) {
        $.post( 
            FOLDERGALLERY_URL+"toggleCatVisibility.php",
            { 
                'aID' : aRefID,
                'state' : state,
                'leptoken' : LEPTOKEN
            },
            function(a,b) {
                if(state === 0)
                {
                    x.prop("title", "inactive");
                } else {
                    x.prop("title", "active");
                }
                
                alert( "Item is now "+( x.prop("title") )+"." );
            }
        );
    }
}