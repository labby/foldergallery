<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');

$section_id = LEPTON_core::getGlobal('section_id');
$page_id = LEPTON_core::getGlobal('page_id');

$cat_id = LEPTON_core::getValue("cat_id", "integer", "get");

if (is_null($cat_id))
{
    LEPTON_admin::getInstance()->print_error('no categorie found', ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
    die();
}

$oFG = foldergallery::getInstance();
$oFG->init_section($page_id, $section_id);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');

LEPTON_handle::include_files ('/modules/foldergallery/functions.php');

$settings = $oFG->fg_settings;
$thumb_size = $settings['thumb_size'];
$root_dir = $settings['root_dir'];
$ratio = $settings['ratio']; 

$database = LEPTON_database::getInstance();

//get infos from db
$categorie = [];
$database->execute_query(
    "SELECT * 
        FROM `".TABLE_PREFIX."mod_foldergallery_categories`
    WHERE `id`=".$cat_id."
    ",
    true,
    $categorie,
    false
);

if (!empty($categorie))
{
    if ($categorie['parent'] != -1 )
    {
        $cat_path = foldergallery::FG_PATH.$settings['root_dir'].$categorie['parent'].'/'.$categorie['categorie'];
        $cat_path = str_replace(LEPTON_PATH, '', $cat_path);
        $parent   = $categorie['parent'].'/'.$categorie['categorie'];
    }
    else
    {
        // Root
        $cat_path = foldergallery::FG_PATH.$settings['root_dir'];
        $parent   = '';
    }
}

$parent_id = $categorie['id'];
$folder = $root_dir.$parent;
$pathToFolder = foldergallery::FG_PATH.$folder.'/';	
$pathToThumb = foldergallery::FG_PATH.$folder.foldergallery::FG_THUMBDIR.'/';
$urlToFolder = foldergallery::FG_URL.$folder.'/';		
$urlToThumb = foldergallery::FG_URL.$folder.foldergallery::FG_THUMBDIR.'/';

$bilder = [];
$database->execute_query(
    "SELECT `id`,`file_name`,`caption` 
        FROM `".TABLE_PREFIX."mod_foldergallery_files` 
    WHERE `parent_id`=".$parent_id." 
        ORDER BY `position`
    ASC
    ",
    true,
    $bilder,
    true
);

if (!empty($bilder))
{
    $info_message = 0;
    foreach ($bilder as $result)
    {
        $bildfilename = $result['file_name'];
        $file = $pathToFolder.$bildfilename;
        
        // a.11
        if (!is_file($file))
        {
            // If the file does not exists, delete the entry from db
                $database->simple_query(
                "DELETE FROM `".TABLE_PREFIX."mod_foldergallery_files`
                 WHERE `id`=".$result['id']."
               "
                ); 
                continue;
        }
        
        // a.12
        $thumb = $pathToThumb.$bildfilename;
            
        if (!is_file($thumb))
        {
            // The thumbnail has to be there
            generateThumb(
                $file,          // origin source
                $thumb,         // target
                $thumb_size,    // size
                1,              // $showmessage
                $ratio
            );
        }
   }
} 
else 
{
    $info_message = 1;
}

$data = array(
    'oFG'           => $oFG,
    'cat_id'        => $cat_id,
    'cat_path'      => $cat_path,
    'categorie'     => $categorie,
    'bilder'        => $bilder,	
    'page_id'       => $_GET['page_id'],
    'section_id'    => $_GET['section_id'],
    'urlToThumb'    => $urlToThumb,
    'info_message'  => $info_message,
    'leptoken'      => get_leptoken()
);

echo $oTWIG->render( 
    "@foldergallery/modify_cat.lte",
    $data
);

LEPTON_admin::getInstance()->print_footer();
