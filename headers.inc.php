<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

LEPTON_handle::register("file_list");
global $page_id;

$lightbox = LEPTON_database::getInstance()->get_one("SELECT lightbox FROM ".TABLE_PREFIX."mod_foldergallery_settings WHERE page_id = ".$page_id);
$ccs_files = file_list(LEPTON_PATH.'/modules/foldergallery/scripts/'.$lightbox.'/css', [], false, "css", LEPTON_PATH.'/', true );
$js_files = file_list(LEPTON_PATH.'/modules/foldergallery/scripts/'.$lightbox.'/js', [], false, "js", LEPTON_PATH.'/', true );

$mod_headers = array(
	'backend' => array(
        'css' => array(
            array(
                'media'  => 'all',
                'file'  => 'modules/lib_fomantic/dist/semantic.min.css'
			)
 		),				
		'js' => array(
			'modules/lib_jquery/jquery-core/jquery-core.min.js',
			'modules/lib_jquery/jquery-core/jquery-migrate.min.js',			
			'modules/lib_fomantic/dist/semantic.min.js'
		)
	),
	'frontend' => array(
		
		'js' => array(
			'modules/lib_jquery/jquery-core/jquery-core.min.js',
			'modules/lib_jquery/jquery-core/jquery-migrate.min.js'
		)
	)	
);

// [1] Load specific lightbox files
foreach($ccs_files as $css)
{
    $mod_headers['frontend']['css'][] = [
        'media'  => 'all',
        'file'  => $css
    ];	
}

foreach($js_files as $js)
{
    $mod_headers['frontend']['js'][] = $js;	
}


// [2] Load only on specific pages
$aTemp = explode( '/', $_SERVER['SCRIPT_NAME']);
$sFilename = array_pop( $aTemp );

if( $sFilename == "modify_thumb.php" )
{
    $mod_headers['backend']['css'][] = [
        'media'  => 'all',
        'file'  => 'modules/foldergallery/js/croppr/dist/croppr.min.css'
    ];
    
    $mod_headers['backend']['js'][] = 'modules/foldergallery/js/croppr/dist/croppr.min.js';
}
