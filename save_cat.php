<?php
/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$oFG = foldergallery::getInstance();
$admin = LEPTON_admin::getInstance();

$save = LEPTON_core::getValue("save", "string");
$bSaveAndBack = false;
if (is_null($save))
{
    $save = LEPTON_core::getValue("saveandback", "string");
    $bSaveAndBack = true;
}

if (!is_null($save))
{
    $cat_id = LEPTON_core::getValue("cat_id", "integer");

    if (is_null($cat_id))
    {
        $admin->print_error('no_cat_id');
    }

    $active = LEPTON_core::getValue("active", "integer") ?? 0;
    $cat_name = LEPTON_core::getValue("cat_name", "string");
    $cat_description = LEPTON_core::getValue("cat_description", "string");

    $fields = array(
        'cat_name'    => $cat_name,
        'description' => $cat_description,
        'active'      => $active
    );

    LEPTON_database::getInstance()->build_and_execute(
        'update',
        TABLE_PREFIX.'mod_foldergallery_categories',
        $fields,
        'id='.$cat_id
    );

    if (!$database->is_error())
    {
        if ($bSaveAndBack == false)
        {
            $admin->print_success($TEXT['SUCCESS'], $oFG->folder_url.'modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$_POST['cat_id']);
        }
        else
        {
            $admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
        }
    }
    else
    {
        $admin->print_error($oFG->language['ERROR_MESSAGE'].": ".$database->get_error(), ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
    }
} else {
    die ('something went wrong');
}

$admin->print_footer();
