<?php
/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$page_id = LEPTON_core::getGlobal('page_id');
$section_id = LEPTON_core::getGlobal('section_id');

$cat_id = -1;

$oFG = foldergallery::getInstance();
$oFG->init_section($page_id, $section_id);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');
LEPTON_handle::include_files ('/modules/foldergallery/functions.php');

if(isset($_POST['edit']) && is_numeric($_POST['edit'])) {
	$file_id    = intval($_POST['edit']);
	$cat_id     = intval($_POST['cat_id']);
	$settings   = $oFG->fg_settings;
	$root_dir   = $settings['root_dir'];

	//get current image data
	$current_image = array();
	LEPTON_database::getInstance()->execute_query(
            "
                SELECT f.`file_name`, f.`parent_id`, c.`categorie` 
                    FROM `".TABLE_PREFIX."mod_foldergallery_files` AS f 
                    JOIN `".TABLE_PREFIX."mod_foldergallery_categories` AS c 
                    WHERE f.`id`=".$file_id." 
                    AND (c.`id` = f.`parent_id`)
            ",
            true,
            $current_image,
            false
	);

        $bildfilename = $current_image['file_name'];

	// get current cat data
	$current_cat = array();
	LEPTON_database::getInstance()->execute_query(
            "
            SELECT `parent`, `categorie`
            FROM `".TABLE_PREFIX."mod_foldergallery_categories`
            WHERE `id`=".$current_image['parent_id']."
            ",
            true,
            $current_cat,
            false
	);
	
    // A.18 - why? ok - get parent path		
	if ($current_cat['parent'] != "-1")
	{
        $parent = $current_cat['parent'].'/'.$current_cat['categorie'];
	}
	else {
	    $parent = '';
	}
		
	$full_file_link = foldergallery::FG_URL.$root_dir.$parent.'/'.$bildfilename;
	$full_file = foldergallery::FG_PATH.$root_dir.$parent.'/'.$bildfilename;
	$thumb_file = foldergallery::FG_PATH.$root_dir.$parent.foldergallery::FG_THUMBDIR.'/'.$bildfilename;

    // make sure the folder exists
    $sBaseCustomPath = __DIR__."/custom_crop_storage/";
    if (!file_exists($sBaseCustomPath))
    {
        LEPTON_core::make_dir($sBaseCustomPath);
    }
    
    $sCustomSettingsFile = $sBaseCustomPath.$file_id.".json";
			
	// check if event handler filled the form
	if(isset($_POST['w']))			
	{
		
		$aBasicValues = [
		    'id'    => $file_id,
		    'x'     => intval($_POST['x']),
		    'y'     => intval($_POST['y']),
		    'w'     => intval($_POST['w']),
		    'h'     => intval($_POST['h'])
		];
		// Force to create a new thumbnail file by deleting the old one.
		LEPTON_handle::delete_obsolete_files ($thumb_file);
        // Create the new one
		$tempResult = generateThumb(
		    $full_file,             // origin source
		    $thumb_file,            // target filename
		    $settings['thumb_size'], // size
		    1,                      // show message
		    $settings['ratio'],     // ratio
		    $aBasicValues['x'],    // left
		    $aBasicValues['y'],    // top
		    $aBasicValues['w'],    // width
		    $aBasicValues['h']     // heigt
		);
	
		if( (true === $tempResult) || ($tempResult > 0) )
		{
			file_put_contents(
			    $sCustomSettingsFile,
			    json_encode( $aBasicValues )
			);
			
			LEPTON_admin::getInstance()->print_success('Thumb erfolgreich geändert', LEPTON_URL.'/modules/foldergallery/modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id);
		} else {
		    
		    // Something has gone wrong!
		    $sMessage = "<p>Error [fg 23]: ".$tempResult.".</p>";
		    switch ($tempResult)
		    {
		        case -1:    // can't create thumb dir
		            $sMessage .= "<p>".$oFG->language['ERROR_THUMBNAILS_CANT_CREATE_DIR']."</p>";
		            break;
		            
		        case -2: // filesize over foldergallery::FG_MEGAPIXEL_LIMIT
		            $sMessage .= "<p>".$oFG->language['ERROR_THUMBNAILS_FILE_TOO_LAGE']."</p>";
		            break;
		            
		        case -3:
		            $sMessage .= "<p>".$oFG->language['ERROR_THUMBNAILS_NO_TYPE_MATCH']."</p>";
		            break;
		            
		        default:
		            $sMessage .= "<p>[fg 237] Unknown result-type.</p>";
		            break;
		    }
		    
		    echo LEPTON_tools::display( $sMessage, "div", "ui message red");
		}
		
	}
	else 
	{
		list($width, $height, $type) = getimagesize($full_file);
			
		// create preview
		if ($settings['ratio'] > 1) {
			$previewWidth   = $settings['thumb_size'];
			$previewHeight  = $settings['thumb_size'] / $settings['ratio'];
		}
		else 
		{
			$previewWidth   = $settings['thumb_size'] * $settings['ratio'];
			$previewHeight  = $settings['thumb_size'];
		}
		
		if(file_exists($sCustomSettingsFile))
		{
		    $bCustom = 1;
		    // keep in mind that we want an array instead of an object here
		    $bCustomSettings = json_decode( file_get_contents( $sCustomSettingsFile ), true) ;
		    
		} else {
            $bCustom = 0;
		    $bCustomSettings = [
		        'x' => 0,
		        'y' => 0,
		        'w' => $previewWidth,
		        'h' => $previewHeight
		    ] ;
		}
		
		$data = array(
			'oFG'               => $oFG,
			'cat_id'            => $cat_id,
			'file_id'           => $file_id,
			'full_file_link'	=> $full_file_link,
			'previewWidth'      => $previewWidth,
			'previewHeight'     => $previewHeight,	
			'page_id'           => intval($_POST['page_id']),
			'section_id'        => intval($_POST['section_id']),
			'relWidth'          => $width,
			'relHeight'         => $height,				
			'leptoken'          => get_leptoken(),
			'custom'            => $bCustom,
			'custom_settings'   => $bCustomSettings
		);
					
		echo $oTWIG->render( 
			"@foldergallery/modify_thumb.lte",
			$data
		);
	}
}
else 
{
	LEPTON_admin::getInstance()->print_error(
	    $oFG->language['ERROR_MESSAGE'],
	    LEPTON_URL.'/modules/foldergallery/modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id
	);
}

LEPTON_admin::getInstance()->print_footer();
