## Foldergallery

### Description
Foldergallery is a module for [LEPTON CMS][1]. 
It's an imagegallery to handle many images in different categories using various jQuery plugins. 
Categories are based on the folderstructure on the server (media-directory = root directory), so they are created
automaticly if you sync the filesystem with the database in the foldergallery backend.

### IMPORTANT
Jquery core and eventually jquery migrate are loaded via get_page_headers to your frontend template (starting with release 3.0).

### Support/Bugs
Please report bugs on the [LEPTON Addon Forum][2], where also support is available.


### Download
Current [installable release][3] can be downloaded on


### Upgrade from Foldergallery JQ, release 2.5.1
- If you run foldergallery_jq < 2.5.1 please update first to 2.5.1, using https://gitlab.com/labby/foldergallery/-/archive/2.5.1/foldergallery-2.5.1.zip
- After that download and unpack the zip file of release 3.0.0 : https://gitlab.com/labby/foldergallery/-/archive/3.0.0/foldergallery-3.0.0.zip
- Upload unzipped foldergallery via FTP into the "modules" directory.  
- Login into the backend and run "upgrade" of foldergallery (NOT foldergallery_jq) in the "modules" section.

#### IMPORTANT
- Outdated" Foldergallery JQ" will automatically renamed to "Foldergallery" if you follow the Upgrade info!
- There is no fallback to outdated module, so backup your files and tables before you start!  
- There are 5 different lighbox scripts included.
- If you want a special interesting lightbox to be included please let us know.

### Changelog
Detailed [Changelog][4] can be seen on [gitlab.com][4] .



[1]: https://lepton-cms.org
[2]: https://forum.lepton-cms.org
[3]: http://www.lepton-cms.com/lepador/modules/foldergallery.php
[4]: https://github.com/labby/foldergallery
