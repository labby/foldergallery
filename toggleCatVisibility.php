<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

header('Content-Type: application/javascript');

$iID = filter_input(INPUT_POST, "aID", FILTER_SANITIZE_NUMBER_INT);
$iState = filter_input(INPUT_POST, "state", FILTER_SANITIZE_NUMBER_INT);

$database = LEPTON_database::getInstance();
if ( ($iState == 0) || ($iState == 1) )
{
    $database->simple_query(
        "UPDATE `".TABLE_PREFIX."mod_foldergallery_categories` set `active` = ? WHERE `id`=?",
        [ $iState, $iID ]
    );
    
    if($database->is_error())
    {
        echo  json_encode($database->get_error() );
    }
} else {
  echo  json_encode("state type doesnt match!");
}
echo "call: ".json_encode($iID);
