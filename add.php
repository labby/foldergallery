<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

/*
 * Add new db entry
 * root_dir is set to module_guid to check, if directory is set
 */
 
 global $page_id, $section_id;
 $oFG = foldergallery::getInstance();
 
 $aFields = array(
    "section_id"    => $section_id,
    "page_id"       => $page_id,
    "root_dir"      => $oFG->module_guid,
    "extensions"    => $oFG->fg_extensions,
    "invisible"     => "",
    "ratio"         => ""
 );
 
 $oFG->database->build_and_execute(
    "insert",
    TABLE_PREFIX . "mod_foldergallery_settings",
    $aFields
);

