<?PHP

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

/**
 *	Getting the foldergallery-settings from the DB for a given section(-id).
 *
 *	@param	int		A valid section-id. Pass by reference.
 *	@return	array	Assoc. array within the settings.
 *
 */
function getSettings( &$section_id )
{
    $sql = 'SELECT * FROM `'.TABLE_PREFIX.'mod_foldergallery_settings` WHERE `section_id` = '.$section_id;
    $result = array();
    LEPTON_database::getInstance()->execute_query( $sql, true, $result, false);
    return $result;
}

/**
 *  Generiert ein Thumbnail $thumb aus $file, falls dieses noch nicht vorhanden ist. 
 *
 *  @param string $file  Pfadangabe zum original File
 *  @param string $thumb Pfadangabe zum Thumbfile
 *  @param integer $thumb_size
 *  @param integer $showmessage
 *  *********************************************
 *  Anpassung für individuelle Thumb erstellung
 *  @param integer $ratio Seitenverhältniss der Thumbs
 *  @param string $positionX Position X von jCrop ansonsten 0
 *  @param string $positionY Position Y von jCrop ansonsten 0
 *  @param string $positionW Position W von jCrop ansonsten 0
 *  @param string $positionH Position H von jCrop ansonsten 0
 *
 *  @return mixed   void or true, negative imntegers for errors
 *
 */
function generateThumb( $file, $thumb, $thumb_size, $showmessage=1, $ratio = 1, $positionX = 0, $positionY = 0, $positionW = 0, $positionH = 0 ){

	//Von Chio eingefügt:
	global $megapixel_limit;
	if ($megapixel_limit < 2)
	{
	    $megapixel_limit = foldergallery::FG_MEGAPIXEL_LIMIT;
	}
	
	static $thumbscounter, $thumbsstarttime, $allthumbssizes;
	if (!$thumbscounter)
	{
	    $thumbscounter = 0;
	}
	if (!$thumbsstarttime)
	{
	    $thumbsstarttime = time();
	}
	if (!$allthumbssizes)
	{
	    $allthumbssizes = 0;
	}
	
	if(!is_file($file) && $showmessage == 1)
	{
        echo '<b>Missing file:</b> '.$file.'<br/>';
        return -1;
	}
	
	$thumbscounter++;
	if ($thumbscounter == 80 && $showmessage == 1)
	{
	    echo '<h3>stopped.. press F5 to reload</h3>';
	}
	if ($thumbscounter > 80)
	{
	    return -1;
	}
	
	if (time() - $thumbsstarttime > 50)
	{
	    die('<h3>timeout.. press F5 to reload</h3>');
	}
	
	$bg = "FFFFFF"; // [3] fixed background?!
	
	$thumbFolder = dirname($thumb);	

	// Verzeichnis erstellen, falls noch nicht vorhanden
	if(!is_dir($thumbFolder))
	{ 
		$u = umask(0);
		if(!mkdir($thumbFolder, 0755)){
			echo '<p style="color: red; text-align: center;">Cant\'t create directory.</p>';
			return -1;
		}
		umask($u);
	}
	
	// Thumb erstellen
	if(!is_file($thumb))
	{

        list($width, $height, $type) = getimagesize($file);
        
        $iTempFileSize = ceil( filesize($file) / 1048576); // 1024 * 1024 
        if( $iTempFileSize > $megapixel_limit)
        {
            if ($showmessage == 1) {
                echo "<br/><b>".$iTempFileSize. " Megabyte filesize! Process skipped!</b>";
            }
            return -2;  // Negative numbers for errors
        }

        $original = NULL;
        
        switch( $type )
        {
            case 1: // gif
                $original = imagecreatefromgif($file);
                break;

            case 2: // jpeg
                $original = imagecreatefromjpeg($file);
                break;

		    case 3: // png
			    $original = imagecreatefrompng($file);
			    break;
			    
			default:
			    echo '<br/><b>No type match!</b>';
			    return -3; // ok, another negative number ...
		}
	
		if ( $original != NULL )
		{
			if ($width >= $height && $width > $thumb_size) {
				//#########
				//Thumbnail verarbeitung verändert um einen Ausschnitt der Größe der $thumbnail_size
				//zu erhalten um ein gleichmäßiges erscheinungsbild im Frontend zu gewährleisten
				//by Pumpi
				//#########
				// Aldus 2021-07-16 - buggy as hell
				$smallwidth = intval($width*$thumb_size/$height);
				$smallheight = $thumb_size;
				$ofx = 0; 
				$ofy = 0;
			} elseif ($width <= $height && $height > $thumb_size) {
				
				$smallheight = intval($height*$thumb_size/$width);
				$smallwidth = $thumb_size;
				$ofx = 0;
				$ofy = 0;
			} else {
				$smallheight = $height;
				$smallwidth = $width;
				$ofx = 0;
				$ofy = 0;
			}
			
			if (function_exists('imagecreatetruecolor')) {
				if ($height > $thumb_size && $width > $thumb_size)
				{
					if ($ratio > 1)
					{
					    $small = imagecreatetruecolor($thumb_size, intval($thumb_size/$ratio));
					}
					else 
					{
					    $small = imagecreatetruecolor(intval($thumb_size*$ratio), $thumb_size);
					}
				}
				else 
				{
					$small = imagecreatetruecolor($smallwidth, $smallheight);
				}
			} 
			else 
			{
				$small = imagecreate($smallwidth, $smallheight);
			}
			
			imagealphablending( $small, false );
            imagesavealpha( $small, true );	
                
			sscanf($bg, '%2x%2x%2x', $red, $green, $blue); // see [3] above
			
			$b = imagecolorallocate($small, $red, $green, $blue);
			imagefill($small, 0, 0, $b);
			if ($original) {
				//Änderungen der Variablen die für JCrop Thumberstellung anderst sein müssen
				if (!empty ($positionW) && !empty($positionH)) {
					$width = $positionW;
					$height = $positionH;
					
					//wenn ein Ratio eingestellt ist werden die small Atribute des Thumbs angepasst
					//die ist allerdings nur bei JCrop nötig normal wird die größe vom 0Punkt aus errechnet
					if ($ratio > 1) {
						$smallwidth = $thumb_size;
						$smallheight = $thumb_size / $ratio;
					}
					else {
						$smallwidth = $thumb_size * $ratio;
						$smallheight = $thumb_size;
					}
				}
				
				if (function_exists('imagecopyresampled')) {
					imagecopyresampled($small, $original, $ofx, $ofy, $positionX, $positionY, $smallwidth, $smallheight, $width, $height);
				} else {
					imagecopyresized($small, $original, $ofx, $ofy, $positionX, $positionY, $smallwidth, $smallheight, $width, $height);
				}
				
			} else {
			    /**
			        Aldus: 2021-07-11
			        is this passage really in use? anywhere?
			        
				$black = imagecolorallocate($small, 0, 0, 0);
				$fw = imagefontwidth($fontSize);
				$fh = imagefontheight($fontSize);
				$htw = ($fw * strlen( $filename ) ) / 2;
				$hts = $thumb_size/2;
				
				imagestring($small, $fontSize, $hts-$htw, $hts-($fh/2), $filename, $black);
				
				imagerectangle($small, $hts-$htw-$fw-1, $hts-$fh, $hts+$htw+$fw-1, $hts+$fh, $black);
			    **/
			    die("call [fg 3004]");
			}
			switch( $type )
			{
			    case 1:
			        imagegif($small, $thumb);
			        break;
			        
			    case 2:
			        imagejpeg($small, $thumb, 95);
			        break;
			        
			    case 3:
			        imagepng($small, $thumb);
			        break;
			        
			    default:
			        // no type match - break?
			        die("[231] No type match!");
			        break;
			}
			imagedestroy($original);
			imagedestroy($small);
			return true;
		}
	}
}
