<?php

declare(strict_types=1);

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
class foldergallery extends LEPTON_abstract
{
    //  Path and url to root directory of Foldergallery, Root directory is the highest level that foldergallery can display
    const FG_PATH = LEPTON_PATH . MEDIA_DIRECTORY; // alternative: LEPTON_PATH;
    const FG_URL = LEPTON_URL . MEDIA_DIRECTORY; // alternative: LEPTON_URL.;
    const FG_THUMBDIR = '/fg-thumbs'; 
    const FG_THUMBDIR1 = 'fg-thumbs'; // The same as above but without "slash", used by search
    const FG_PAGES = PAGES_DIRECTORY;    
    const FG_MEGAPIXEL_LIMIT = 6; //  Max MB for thumbnails: if an image-file size is greater than this value (in MB) no thumbnails are generated.
    const CORE_FOLDERS = array('account', 'admins', 'framework', 'include', 'languages', 'modules', self::FG_PAGES, 'search', 'temp', 'templates');
    const INVISIBLE_FILE_NAMES = array('.', '..', self::FG_THUMBDIR1);

    public int $page_id = 0;
    public int $section_id = 0;
    public array $fg_settings = [];
    public array $fg_category_all = [];
    public string $addon_color = 'orange';
    public string $fg_extensions = 'jpg,jpeg,gif,png';
    public string $folder_url = LEPTON_URL . '/modules/foldergallery/';
    public string$modify_url = ADMIN_URL . '/pages/modify.php';
  
    public LEPTON_database $database;
    public LEPTON_admin $admin;
    public static $instance; // Own instance of the class

    public function initialize() 
    {
        $this->database = LEPTON_database::getInstance();
        $this->admin = LEPTON_admin::getInstance();		
    }

    public function init_section(int|NULL $iPageID = 0, int|NULL $iSectionID = 0) 
    {
        $this->page_id = $iPageID;
        $this->section_id = $iSectionID;

        // get an array of settings
        LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `" . TABLE_PREFIX . "mod_foldergallery_settings` WHERE `section_id`=" . $iSectionID,
            true,
            $this->fg_settings,
            false
        );

        //get all categories on section
        LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `" . TABLE_PREFIX . "mod_foldergallery_categories` WHERE `section_id`=" . $iSectionID . " ORDER BY `position` ",
            true,
            $this->fg_category_all,
            true
        );
    }

    public function toggle_active( int $id) 
    {   // Function to switch between active/inactive

        $data = $this->database->get_one("SELECT `active` FROM `" . TABLE_PREFIX . "mod_foldergallery_categories` WHERE `id` = " . $id);

        $new = ( $data == 1 ) ? 0 : 1;

        $this->database->simple_query("UPDATE `" . TABLE_PREFIX . "mod_foldergallery_categories` SET `active`=" . $new . " WHERE `id` = " . $id);

        // Say success
        $this->admin->print_success($this->language['SAVE_SETTINGS'], $this->modify_url.'?page_id=' . $this->page_id );
    }

    /**
     *  Handle the 'move' of given entries "up" or "down" (the position).
     *
     *  @return boolean True if success, otherwise false.
     */
    public function move() : bool 
    {
        $order = new LEPTON_order(TABLE_PREFIX . "mod_foldergallery_categories", 'position', 'id', 'parent_id');

        $iDirection_up   = LEPTON_core::getValue("move_up", "integer") ?? 0;
        $iDirection_down = LEPTON_core::getValue("move_down", "integer") ?? 0;
        
        $bSuccess = false;
        
        if ($iDirection_up > 0)
        {
            $bSuccess = $order->move_up($iDirection_up);
        }
        
        if ($iDirection_down > 0)
        {
            $bSuccess = $order->move_down($iDirection_down);
        }
        
        if (true === $bSuccess)
        {
            $this->admin->print_success(
                $this->language['SAVE_SETTINGS'],
                $this->modify_url.'?page_id=' . $this->page_id
            );
            return true;
        } 
        else 
        {
            $this->admin->print_error($this->language['DB_ERROR'] . "<br />" . $order->errorMessage, ADMIN_URL . '/pages/modify.php?page_id=' . $this->page_id);
            return false;
        }
    }

    // A.14

    /**
     *  Builds the catalog tree in a given array
     *
     *  @param  integer $iParentID  A valid parent-id value >= 0.
     *  @param  array   $aStorrage  An array to store the results. Pass by reference!.
     *  @param  integer $iDeep      An internal recursition counter.
     *
     */
    public function buildCatTree($iParentID = 0, &$aStorrage = [], $iDeep = 0) {
        foreach ($this->fg_category_all as &$ref) {
            if ($ref['parent_id'] == $iParentID) {
                $aTemp = [];
                $this->buildCatTree($ref['id'], $aTemp, $iDeep + 1);
                $ref['subcategories'] = $aTemp;

                $aStorrage[] = $ref;
            }
        }
    }

}
