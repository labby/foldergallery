<?php

declare(strict_types=1);

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
class foldergallery_frontend extends LEPTON_abstract_frontend
{
    public string $view_url     = LEPTON_URL.PAGES_DIRECTORY;
    public string $thumb_url    = "";
    public string $lb_url       = "";   // lightbox URL
    public array $settings      = [];
    public array $categories    = [];

	public LEPTON_database $database;
	public static $instance;

    public function initialize() 
    {
         // needed by (abstract) parent
        $this->database = LEPTON_database::getInstance();

    }

    public function init_frontend_page(int|NULL $iPageID = 0, int|NULL $iSectionID = 0) 
    {
        // get page link
        $page_link = $this->database->get_one("SELECT `link` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".$iPageID);

        $this->view_url = LEPTON_URL.PAGES_DIRECTORY.$page_link.PAGE_EXTENSION;	

        // get array of settings
        $this->database->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_settings` WHERE `section_id`=".$iSectionID." ",
            true,
            $this->settings,
            false
        );

        // get all categories on section
        $this->database->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `section_id`=". $iSectionID." ORDER BY `position` ",
            true,
            $this->categories,
            true
        );

        // get url
        $this->thumb_url = foldergallery::FG_URL.$this->settings['root_dir'].foldergallery::FG_THUMBDIR;
        $this->lb_url = foldergallery::FG_URL.$this->settings['root_dir'];
    }

    /**
     *
     *  @param  integer $iParentID   The "root" id looking for
     *  @param  array   $aStorage    An array for the results - pass by reference!
     *  @param  integer $iDeep       The recursions counter.
     *
     */
    public function buildCatTreeFrontend(int $iParentID = 0, array &$aStorage = [], int $iDeep = 0)
    {
        foreach ($this->categories as &$ref) 
        {
            if ($ref['parent_id'] == $iParentID) 
            {
                $aTemp = [];
                $this->buildCatTreeFrontend($ref['id'], $aTemp, $iDeep + 1);
                $ref['subcategories'] = $aTemp;

                // Files:
                $aAllFilesForCategory = [];
                $this->database->execute_query(
                        "SELECT * from `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `parent_id`=" . $ref['id']." ORDER BY `position`",
                        true,
                        $aAllFilesForCategory,
                        true
                );
                $ref['files'] = &$aAllFilesForCategory;

                $aStorage[] = $ref;
            }
        }
    }

    /**
     * [A.7]
     * @param int $iID
     * @param array $aStorrage
     */
    public function buildCatTreeFrontendByID(int $iID = 0, array &$aStorrage = [])
    {
        foreach ($this->categories as &$ref) 
        {
            if ($ref['id'] == $iID) 
            {
                $aTemp = [];
                
                $ref['subcategories'] = $aTemp;

                // Files:
                $aAllFilesForCategory = [];
                $this->database->execute_query(
                        "SELECT * from `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `parent_id`=" . $ref['id'] . " ORDER BY `position` ", //LIMIT " . $this->settings['pics_pp'],
                        true,
                        $aAllFilesForCategory,
                        true
                );
                $ref['files'] = &$aAllFilesForCategory;

                $aStorrage[] = $ref;
            }
        }
    }

    /**
     * [A.8]
     * @param int $iSectionID
     * @return int
     */
    public function getCatID(int $iSectionID = 0) : int
    {
        $iCatID = -1;
        $sTempCatStr = filter_input(INPUT_GET, 'cat', FILTER_SANITIZE_SPECIAL_CHARS);
        
        if ($sTempCatStr  != "")
        {
            $sTempCat = substr($sTempCatStr, 1); // ignor the first char ("/")
            $aTemp = explode("/", $sTempCat);

            $sTempCat = array_pop($aTemp); // we want the LAST element

            $sTempCatId = $this->database->get_one("SELECT `id` FROM `" . TABLE_PREFIX . "mod_foldergallery_categories` WHERE `categorie`='" . $sTempCat . "' AND `section_id`='" . $iSectionID . "'");

            if ($sTempCatId != NULL) {
                $iCatID = $sTempCatId;
            }
        }
        return $iCatID;
    }
}
