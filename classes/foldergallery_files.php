<?php

declare(strict_types=1);

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
class foldergallery_files extends LEPTON_abstract
{
    public array $settings = [];
    public array $aDirectories = [];
	
	public LEPTON_database $database;
	public static $instance;

    public function initialize()
    {
        // needed by (abstract) parent.
		$this->database = LEPTON_database::getInstance();
    }

    /**
     * Aktuelle Positionen neu Durchnummerieren
     * @return bool
     */
    public static function repairPositions() : bool
    {

        $mySelf = self::getInstance();

        $aAllCatIDs = [];
        
        // 1.0 Test

        $iTest = $mySelf->database->get_one("SELECT `id` FROM `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `position`=0");
        if (NULL === $iTest)
        {
            return false;
        }
        
        // 2.0 Alle CatalogIds
        $mySelf->database->execute_query(
            "
            SELECT DISTINCT `parent_id`, `position`
            FROM `".TABLE_PREFIX."mod_foldergallery_files`
            ORDER by `parent_id`,`position`
            ",    
            true,
            $aAllCatIDs,
            true
        );
        
        // 3.0 Alle CatIDs die Position neu Durchnummerieren
        foreach($aAllCatIDs as $iTempCatId)
        {
            // 3.1 Startposition
            $iTempPosition = 1;
            
            // 3.2 datensätze holen
            $aTempFiles = [];
            $mySelf->database->execute_query(
                "
                SELECT `id`, `position` 
                FROM `".TABLE_PREFIX."mod_foldergallery_files` 
                WHERE `parent_id`=".$iTempCatId["parent_id"]." ORDER BY `position`
                ",
                true,
                $aTempFiles,
                true
            );
            
            // 3.3 Aktualisieren
            foreach($aTempFiles as $iCurrentID)
            {
                $mySelf->database->simple_query(
                    "UPDATE `".TABLE_PREFIX."mod_foldergallery_files`
                     SET `position`= ".($iTempPosition++)." WHERE `id`=".$iCurrentID["id"]
                );
            }
            
        }      
        return true;
    }

    /**
     * @param int $section_id
     * @return bool
     */
    public function checkThumbnails(int $section_id = 0): bool
    {
        // now valid values?
        if ($section_id === 0)
        {
            return false;
        }

        // 1 load functions
        LEPTON_handle::include_files ('/modules/foldergallery/functions.php');
        LEPTON_handle::register("make_dir");
        
        // [2] get current gallery settings via section_id
        // [2.1] the fields
        $aLookForFields = ['section_id','page_id','root_dir','thumb_size','ratio'];
        // [2.2] result storage
        $aSettings = [];
        $this->database->execute_query(
            "SELECT `".(implode("`,`",$aLookForFields))."`
                FROM `".TABLE_PREFIX."mod_foldergallery_settings`
                WHERE `section_id`=".$section_id,
            true,
            $aSettings,
            false
        );

        // [2.3] no settings found? return false
        if (empty($aSettings))
        {
            return false;
        }
        
        // [3] build the basic paths
        $sBasicFolderPath = LEPTON_PATH.MEDIA_DIRECTORY.$aSettings["root_dir"];
        
        // [4] get a list of categories/folders/cat_ids
        $aAllCategories = [];
        $this->database->execute_query(
            "SELECT `id`, `parent`, `parent_id`, `childs`,`categorie` FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `section_id`=".$section_id,
            true,
            $aAllCategories,
            true
        );
        
        // [4.1] any results?
        if(empty($aAllCategories))
        {
            return false;
        }
        
        // [5] foreach cat
        foreach ($aAllCategories as &$tempCat)
        {
            // [5.0.1] Root?
            if ($tempCat["categorie"] == "Root")
            {
                $tempCat["categorie"] = "";
                $tempCat["parent"] = "";
            }
            // [5.0.2]
            if (($tempCat["categorie"] != "") && ($tempCat["categorie"][0] != "/"))
            {
                $tempCat["categorie"] = "/".$tempCat["categorie"];
            }
            // [5.1] - get corresponding subfolders to the thumbNailFolders
            if ($tempCat["parent"] != "")
            {
                $sThumbnailFolder = $sBasicFolderPath.$tempCat["parent"].$tempCat["categorie"].foldergallery::FG_THUMBDIR;
                $sSourceFolder = $sBasicFolderPath.$tempCat["parent"].$tempCat["categorie"];
            } 
            else 
            {
                $sThumbnailFolder = $sBasicFolderPath.$tempCat["categorie"].foldergallery::FG_THUMBDIR;
                $sSourceFolder = $sBasicFolderPath.$tempCat["categorie"]; 
            }
            echo "<p>".$sSourceFolder."</p>";
            // [5.1.1] add slash
            $sSourceFolder .= "/";
                        
            // [5.2] - get all relevant image-names for the current cat
            $aTempImages = [];
            $this->database->execute_query(
                    "SELECT `file_name` FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `parent_id` =".$tempCat["id"],
                    true,
                    $aTempImages,
                    true
            );
            
            // [5.2.1] Any files and does the directory exists? If not, create it
            if ((!empty($aTempImages)) && (false === is_dir($sThumbnailFolder)))
            {
                LEPTON_core::make_dir($sThumbnailFolder);
            }
            
            // [5.2.1] - foreach img
            foreach ($aTempImages as $aTempFile)
            {
                $sFile = $sThumbnailFolder."/".$aTempFile['file_name'];
                
                // [5.2.1.1] - is the thumbnail there?
                if (!file_exists($sFile))
                {
                    // [5.2.1.1.1] - No -> generate
                    generateThumb(
                        $sSourceFolder.$aTempFile["file_name"],  // origin source
                        $sFile,                                 // target
                        $aSettings["thumb_size"],   // size
                        1,                          // $show message
                        $aSettings["ratio"]
                    );   
                } 
                else 
                {

                    // [5.2.1.2] - has the thumbnail the correct dimensions?
                    $aTempImageInfo = getimagesize($sFile);
                    // [0] == width
                    // [1] == height
                    if (($aTempImageInfo[0] != $aSettings["thumb_size"]) || ($aTempImageInfo[1] != $aSettings["thumb_size"]))
                    {
                        // [5.2.1.2.1] - No -> generate a new one
                        generateThumb(
                            $sSourceFolder.$aTempFile["file_name"], // origin source
                            $sFile,                                     // target
                            $aSettings["thumb_size"],                   // size
                            1,                              // show message
                            $aSettings["ratio"]
                        );
                    }
                        
                }       
            }
        }        
        // [6] return true|false
        return true;
    }
}
