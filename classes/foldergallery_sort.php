<?php

declare(strict_types=1);

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
class foldergallery_sort extends LEPTON_abstract
{

    public string $errorMessage = "";
	
	public LEPTON_database $database;	
	public static $instance;

	public function initialize() 
	{
        // needed by (abstract) parent
		$this->database = LEPTON_database::getInstance();
	}
	
	public function validate_post()
	{
	    $requested = [
	        'shash'         => "phppass",
	        'section_id'    => "section",
	        'newList'       => "list"
	    ];
	    
	    foreach($requested as $key => $type )
	    {
	        
	        if(!isset($_POST[ $key ]))
	        {
	            $this->errorMessage = "[1] Key";
	            return false;
	        } else {
	            $value = $_POST[ $key ];
	        }
	        
	        switch( strtolower($type) )
	        {
	            case 'phppass':
	                if(false === password_verify ( LEPTON_GUID , $value ) )
	                {
	                    $this->errorMessage = "[2] Hash";
	                    return false;
	                }
	                break;
	                
	            case 'section':
	                // Is the section_id valid? (e.g. does a section with this number existes?
	                $result = $this->database->get_one("SELECT `page_id` FROM `".TABLE_PREFIX."sections` WHERE `section_id`=".intval($value)." AND `module`='foldergallery';"); 
	                if( NULL === $result)
	                {
	                    $this->errorMessage = "[3] Section";
	                    return false;
	                }
	                break;
	                
	            case "list":
	                if(!is_array($value))
	                {
	                    $this->errorMessage = "[4] List";
	                    return false;
	                }
	                foreach($value as $item)
	                {
	                    if(!is_int( intval($item)))
	                    {
	                        $this->errorMessage = "[4.1] Integer";
	                        return false;
	                    } else {
	                    
	                        $test = $this->database->get_one("SELECT `file_name` FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `id` = ".$item);
	                        if(NULL === $test)
	                        {
	                            $this->errorMessage = "[4.2] other";
	                            return false;
	                        }
	                    }
	                }
	                break;
	                
	            default:
	                $this->errorMessage = "[5] unknown";
	                return false;
	                break;
	        }
	    }
	    
	    return true;
	}

    public function update_sort()
    {
        $myList = $_POST['newList'];
        $iPosition = 1;
        foreach ($myList as $item)
        {
            $this->database->simple_query(
                "UPDATE `".TABLE_PREFIX."mod_foldergallery_files` set `position` = ".($iPosition++)." WHERE `id` = ".$item
            );            
        }
        return true;
    }
} // end of class
