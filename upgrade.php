<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php



$database = LEPTON_database::getInstance();
$release = $database->get_one("SELECT version from ".TABLE_PREFIX."addons WHERE directory = 'foldergallery' ");

if($release <'3.6.0')
{
	// force to new install
	die(LEPTON_tools::display('No Upgrade possible!<br>Please uninstall foldergallery and then install Addon again!', 'pre','ui red message'));
}

if($release <= '3.6.1')
{
	// delete obsolete files
	LEPTON_handle::delete_obsolete_directories(['/modules/foldergallery/custom_crop_storage']);	
	LEPTON_handle::delete_obsolete_files('/modules/foldergallery/.gitignore');
}

