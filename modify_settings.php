<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$oFG = foldergallery::getInstance();
LEPTON_handle::include_files ('/modules/foldergallery/backend.functions.php');
$oFG->init_section( $page_id, $section_id );
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');

// prepare folders to show
if ( ! empty( $oFG->fg_settings['invisible'] ) ) {
    $invisibleFileNames = array_merge( foldergallery::INVISIBLE_FILE_NAMES, explode( ',', $oFG->fg_settings['invisible'] ) );
}
// Do not display system directories
$invisibleFileNames = array_merge(foldergallery::INVISIBLE_FILE_NAMES, foldergallery::CORE_FOLDERS);

LEPTON_handle::register( "directory_list");
$aTempFolders = array();
$sStripFromPath = LEPTON_PATH.MEDIA_DIRECTORY."/";

directory_list(
    LEPTON_PATH.MEDIA_DIRECTORY."/", // leading slash is ugly! (old mess)
    false,
    0,
    $aTempFolders,
    $sStripFromPath
);

// Strip the thumbnail(-dirs) from the list
$folders = array();
foreach($aTempFolders as &$ref)
{
    $aTemp = explode("/", $ref);
    if( array_pop($aTemp) != foldergallery::FG_THUMBDIR1 )
    {
        $folders[] = $ref;
    }
}

//Ratio array
$ratio = array(
	$oFG->language['Ration_square'] => 1, 
	"4:3" => round(4/3, 4), 
	"3:4" => round(3/4, 4), 
	"16:9" => round(16/9, 4), 
	"9:16" => round(9/16, 4)
);

// [3] available lightboxes
LEPTON_handle::register("file_list");
$aFiles = file_list(
    dirname(__FILE__).'/templates/frontend',
    ["overview.lte", "head_lightbox.lte", "foot_lightbox.lte"],
    false,
    "lte",
    dirname(__FILE__).'/templates/frontend/'    
);
//  [3.1] Build select options (HTML-source string)
$lightbox_select = '';
foreach($aFiles as $file)
{
    if ( preg_match( "/^view_(\w+).lte$/", $file, $matches ) )
    {
        $lightbox_select .= '<option value="'.$matches[1] .'"';
        if ( $matches[1] == $oFG->fg_settings['lightbox'] )
        {
            $lightbox_select .= ' selected="selected"';
        }
        $lightbox_select .= '>'.$matches[1].'</option>';
    }
}
$lightbox_select .= '';
	
$data = array(
	'oFG'         => $oFG,
	'page_id'     => $page_id,
	'section_id'  => $section_id,
	'folders'     => $folders,
	'lightbox'    => $lightbox_select,		
	'ratio'       => $ratio,	
	'leptoken'    => get_leptoken()
);
		
echo $oTWIG->render( 
	"@foldergallery/modify_settings.lte",	//	template-filename
	$data   //	template-data
);

LEPTON_admin::getInstance()->print_footer();
