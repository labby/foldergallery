<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$oFG = foldergallery::getInstance();
$section_id = LEPTON_core::getGlobal('section_id');
$page_id = LEPTON_core::getGlobal('page_id');
$oFG->init_section($page_id, $section_id);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');

LEPTON_handle::include_files ('/modules/foldergallery/backend.functions.php');

$thumb_size = $oFG->fg_settings['thumb_size'];
$root_dir = $oFG->fg_settings['root_dir'];

if(isset($_GET['cat_id']) && is_numeric($_GET['cat_id']))
{
    $cat_id = filter_input(INPUT_GET, "cat_id", FILTER_SANITIZE_NUMBER_INT);
} else {
    LEPTON_admin::getInstance()->print_error('no categorie found', ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
}

//get infos from db
$categorie = array();	
LEPTON_database::getInstance()->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE id=".$cat_id,
    true,
    $categorie,
    false
);


if ( !empty($categorie) )
{
    if ( $categorie['parent'] != -1 )
    {
        $cat_path = foldergallery::FG_PATH.$oFG->fg_settings['root_dir'].$categorie['parent'].'/'.$categorie['categorie'];
        $parent   = $categorie['parent'].'/'.$categorie['categorie'];
    }
    else
    {
        // Root
        $cat_path = foldergallery::FG_PATH.$oFG->fg_settings['root_dir'];
        $parent   = '';
    }
}

$parent_id = $categorie['id'];
$folder = $root_dir.$parent;
$pathToFolder = foldergallery::FG_PATH.$folder.'/';	
$pathToThumb = foldergallery::FG_PATH.$folder.foldergallery::FG_THUMBDIR.'/';
$urlToFolder = foldergallery::FG_URL.$folder.'/';		
$urlToThumb = foldergallery::FG_URL.$folder.foldergallery::FG_THUMBDIR.'/';

$bilder = array();	
LEPTON_database::getInstance()->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `parent_id`=".$parent_id." ORDER BY position ASC",
	true,
	$bilder,
	true
);

$data = array(
	'oFG'           => $oFG,
	'cat_id'	=> $cat_id,
	'cat_path'	=> $cat_path,
	'categorie'	=> $categorie,
	'bilder'	=> $bilder,	
	'page_id'	=> filter_input(INPUT_GET, "page_id", FILTER_SANITIZE_NUMBER_INT), // $_GET['page_id'],
	'section_id'    => filter_input(INPUT_GET, "section_id", FILTER_SANITIZE_NUMBER_INT), // $_GET['section_id'],
	'time'		=> time(),
	'urlToThumb'    => $urlToThumb,	
	'leptoken'	=> get_leptoken(),
	'shash'         => password_hash( LEPTON_GUID, PASSWORD_DEFAULT)
);
		
echo $oTWIG->render( 
    "@foldergallery/modify_cat_sort.lte",   //	template-filename
    $data                                           //  template-data
);

LEPTON_admin::getInstance()->print_footer();
