<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


$oFG = foldergallery::getInstance();
$database = LEPTON_database::getInstance();
$admin = LEPTON_admin::getInstance();

$save = $admin->getValue("save", "string");
$bSaveAndBack = false;
if (is_null($save))
{
    $save = $admin->getValue("saveandback", "string");
    $bSaveAndBack = true;
}

if (!is_null($save))
{
    // use $_POST
    $cat_id = $admin->getValue("cat_id", "integer", "get");

    if (is_null($cat_id))
    {
        $admin->print_error('lost cat', ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
        die();
    }

    $aCaptions = $admin->getValue("caption", "array");

    if (is_null($aCaptions))
    {
        $admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/foldergallery/modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id);
    }

    foreach ($aCaptions as $key => $value)
    {
        $database->simple_query(
            "Update `".TABLE_PREFIX."mod_foldergallery_files` SET `caption` = :imagecaption WHERE `id`= :imageid",
                [
                    'imagecaption' => $value,
                    'imageid'      => $key
                ]
            );

    }

    if ($bSaveAndBack == false)
    {
        $admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/foldergallery/modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id);
    } else {
        $admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id);
    }
}
else 
{
    $admin->print_error('wrong data!');
}
