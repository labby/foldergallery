<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$oFGF = foldergallery_frontend::getInstance();
$oFGF->init_frontend_page($page_id, $section_id);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');

// Foldergallery settings
$settings 	= $oFGF->settings;

$root_dir   = $settings['root_dir'];
$catpic     = intval($settings['catpic']);
$ratio      = $settings['ratio']; 

// Build the category tree
$iCurrentCatID = $oFGF->getCatID( $section_id );

$aCatTree = [];
if($iCurrentCatID === -1)
{
    $oFGF->buildCatTreeFrontend( -1, $aCatTree, 0);
} 
else 
{
    $oFGF->buildCatTreeFrontendByID( $iCurrentCatID, $aCatTree, 0);
    
    $sPathAddion = ( isset( $aCatTree[0]['parent_id']) && ($aCatTree[0]['parent_id'] != 0) )
        ? $aCatTree[0]['parent']
        : ""
        ;
}

$result = [];
$subCat = [];
$images = [];
$title = PAGE_TITLE;

// only subpages/subfolder
if( (isset($_GET['cat'])) && (is_string($_GET['cat'])))
{
    $currentCat = filter_input(INPUT_GET, "cat", FILTER_SANITIZE_SPECIAL_CHARS);
} 
else 
{
    $currentCat = '';
}


$all_cats = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `section_id` = ".$section_id." AND `is_empty` = 0 AND `active` = 1 ORDER BY `position` DESC ",
    true,
    $all_cats,
    true
);


$sRootTitle   		= $database->get_one("SELECT cat_name FROM ".TABLE_PREFIX."mod_foldergallery_categories WHERE parent = -1 AND section_id = ".$section_id);
$sRootDescription   = $database->get_one("SELECT description FROM ".TABLE_PREFIX."mod_foldergallery_categories WHERE parent = -1 AND section_id = ".$section_id);
$currentCat_id 		= $database->get_one("SELECT id FROM ".TABLE_PREFIX."mod_foldergallery_categories WHERE parent = -1 AND section_id = ".$section_id);

//  Display root if there are no subfolders
if (("" == $currentCat) || ($iCurrentCatID == -1 ))
{
    $sql = "
        SELECT *
        FROM `" . TABLE_PREFIX . "mod_foldergallery_categories`
        WHERE   `section_id`=" . $section_id . "
            AND `parent`    != -1
            AND `active`    = 1
        ORDER BY `position` ASC
        ;";
} 
else 
{
    $sql = "
        SELECT *
        FROM `" . TABLE_PREFIX . "mod_foldergallery_categories`
        WHERE   `section_id`=" . $section_id . "
            AND `parent`    = '" . $currentCat . "'
            AND `active`    = 1
        ORDER BY `position` ASC
        ;";
}

$subCats = [];
$database->execute_query(
    $sql,
    true,
    $subCats,
    true
);

if (!empty($subCats) && $subCats[0]['parent_id'] != -1) 
{
    // get thumb (file_name) from files table
    foreach ($subCats as &$thumb) {
        switch (intval($catpic)) {
            case 2: // last
                $cat_thumb = $database->get_one("SELECT `file_name` FROM `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `parent_id` =" . $thumb['id'] . " ORDER BY position DESC");
                break;

            case 1: // first
                $cat_thumb = $database->get_one("SELECT `file_name` FROM `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `parent_id` =" . $thumb['id'] . " ORDER BY position ASC");
                break;

            case 0: // random
                $all_thumbs = [];
                $database->execute_query(
                     "SELECT `file_name`
                          FROM `" . TABLE_PREFIX . "mod_foldergallery_files`
                      WHERE `parent_id` =" . $thumb['id'] . "
                          ORDER BY `position`",
                    true,
                    $all_thumbs,
                    true
                );

                if (empty($all_thumbs)) 
				{
                    die(LEPTON_tools::display("[fg fe 2] Fatal error: no thumbnails! " . $thumb['parent_id'], "pre", "ui message orange"));
                } 
				else 
				{
                    $cat_thumb = array_rand($all_thumbs);
                    $cat_thumb = $all_thumbs[$cat_thumb]["file_name"];
                }

                break;

            default:
                die(LEPTON_tools::display("[fg fe 1] Unknown case to handle the images " . $catpic, "pre", "ui message red"));
                break;
        }

        $thumb['cat_thumb'] = $cat_thumb;
    }
}

$breadcrumb = "";
// create breadcrumb, get current cat path (only for subgalleries, not root)
if ( (isset( $_GET['cat'] ) && ($currentCat_id != "") ) || ($iCurrentCatID != -1) )
{
    $path  = explode( '/', filter_input(INPUT_GET, "cat", FILTER_SANITIZE_SPECIAL_CHARS) );

    // first element is empty as the string begins with /
    array_shift($path);
    foreach ( $path as $i => $cat_name )
    {
        $cat = $database->get_one(
            "
            SELECT `cat_name` 
                FROM `".TABLE_PREFIX."mod_foldergallery_categories` 
            WHERE `categorie` = '".$cat_name."' 
                AND `section_id`='".$section_id."'
                LIMIT 1
            "
        );
        
        $breadcrumb .= "<li> <a href='".$oFGF->view_url ."?cat=/".implode('/', array_slice( $path, 0, ($i+1) ) ) .  "'>".$cat."</a></li>";
    }
    $bread_id = 1;

} 
else 
{
    $bread_id = 0;
}

if($iCurrentCatID == -1)
{
    $bread_id = 0;
}
$lookUpId = ($iCurrentCatID == -1) ? $oFGF->categories[0]['id'] :  $iCurrentCatID;
// Figure out how many pages are needed to display all the thumbs
// Aldus: 2022-05-11 ==> $oFGF->categories[0] seems to be wrong if we are in a subcategorie!
$all_images = [];
$database->execute_query(
    "SELECT * FROM `" . TABLE_PREFIX . "mod_foldergallery_files` WHERE `parent_id` = " . $lookUpId, // $oFGF->categories[0]['id'],
    true,
    $all_images,
    true
);
//die(LEPTON_tools::display($oFGF->categories, "pre", "ui message red"));
// Aldus: 2022-05-11
// Bug bei (Unter-)Kategorien
$count_images = intval(count($all_images));
if ($count_images > $oFGF->settings['pics_pp'] ) 
{
	$pages = ceil( count($all_images) / intval($oFGF->settings['pics_pp']));
}
else
{
	$pages = 1;
}


$start_image_number = 0;
// if we expect MORE than one page AND we found a "p=yyy" in the requested url then::
$iTempPageNumber = 1; // init with 1 
if( ($pages > 1) && (isset($_GET["p"])) && (is_numeric($_GET["p"])))
{
    $iTempPageNumber = intval($_GET["p"]);
	
    if($iTempPageNumber > 1)
    {
        // Calculate the start-value for the twig-output new!
        $start_image_number = ($iTempPageNumber-1) * intval($settings['pics_pp']);
    }  
}

/**
 * 3.1 render "head" with title
 */
$data_head = [
    'oFGF'      => $oFGF,
    'catTree'   => $aCatTree[0],  // root der Gallery
    'subCats'   => $subCats,
    'pages'     => $pages,
    'currentCat' => $currentCat,
    'thumb_name'=> foldergallery::FG_THUMBDIR
];

/**
 * 3.2 render lightbox source
 */
$data_lb = [
    'oFGF'          => $oFGF,
    'page_id'       => $page_id,
    'section_id'    => $section_id,
    'bread_id'      => $bread_id,	
    'breadcrumb'    => $breadcrumb,
    'thumb_name'    => foldergallery::FG_THUMBDIR,
    'catTree'       => $aCatTree[0],  // root der Gallery
    'page_title'    => PAGE_TITLE,
    'pages'         => $pages,
    'currentCat'    => $currentCat,
    'count_images'  => $count_images,
    'pics_per_page'  	 => $oFGF->settings['pics_pp'],	
    'rootDescription'	 => $sRootDescription,
    'rootTitle'			 => $sRootTitle, 
    'start_image_number' => $start_image_number, //
    'current_pagenumber' => $iTempPageNumber
];

// render overview and galleries, set hook for custom templates
if($aCatTree[0]['has_child'] == 1 )
{
	if(file_exists(LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/foldergallery/overview.lte'))
	{
		echo $oTWIG->render( 
			"@foldergallery/overview.lte",
			$data_head
		);		
	}
	else
	{
		echo $oTWIG->render( 
			"@foldergallery/frontend/overview.lte",
			$data_head
		);	
	}
}
else
{
	if(file_exists(LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/foldergallery/view_'.$settings['lightbox'].'.lte'))
	{
		echo $oTWIG->render( 
			"@foldergallery/view_".$settings['lightbox'].".lte",
			$data_lb
		);		
	}
	else
	{
		echo $oTWIG->render( 
			"@foldergallery/frontend/view_".$settings['lightbox'].".lte",
			$data_lb
		);	
	}	
}
