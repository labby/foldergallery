<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// get class instance
$oFG = foldergallery::getInstance();

$cat_id = filter_input(INPUT_POST, "cat_id", FILTER_SANITIZE_NUMBER_INT);
if (0 === $cat_id) {
    LEPTON_admin::getInstance()->print_error(
        'No category id submitted',
        ADMIN_URL . '/pages/modify.php?page_id=' . $page_id . '&section_id=' . $section_id
    );
    die();
}

$page_id = filter_input(INPUT_POST, "page_id", FILTER_SANITIZE_NUMBER_INT);

$section_id = filter_input(INPUT_POST, "section_id", FILTER_SANITIZE_NUMBER_INT);

$sSort = filter_input(INPUT_POST, "section_id", FILTER_SANITIZE_SPECIAL_CHARS);

if ($sSort != "")
{
    switch ($_POST['sort']) {
        case "ASC":
            $sort = "ASC";
            break;
        case "DESC":
            $sort = "DESC";
            break;
        default:
            LEPTON_admin::getInstance()->print_error('No sort advice [1]');
            break;
    }
} else {
    LEPTON_admin::getInstance()->print_error('No sort advice [2]');
}

// get infos from db
$result = array();	
LEPTON_database::getInstance()->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_files`  WHERE `parent_id` =".$cat_id." ORDER BY `file_name` ".$sort,
    true,
    $result,
    true
);	

if(count($result) > 0) {
    $sql = "UPDATE `".TABLE_PREFIX."mod_foldergallery_files` SET `position`= CASE ";
    $position = 1;
    foreach($result as $image){
        $sql = $sql."WHEN id=".$image['id']." THEN '".$position."' ";
	$position++;
    }
    $sql = $sql." ELSE position END;";
}

if(LEPTON_database::getInstance()->query($sql))
{
    LEPTON_admin::getInstance()->print_success(
        $oFG->language['IMAGES_REARRANGED'],
        LEPTON_URL.'/modules/foldergallery/modify_cat_sort.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id
    );
} else {
    LEPTON_admin::getInstance()->print_error(
        $TEXT['ERROR'],
        LEPTON_URL.'/modules/foldergallery/modify_cat_sort.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id
    );
}

// Print admin footer
LEPTON_admin::getInstance()->print_footer();
