<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

//global $page_id, $section_id;
//$page_id = LEPTON_core::getGlobal('page_id');
//$section_id = LEPTON_core::getGlobal('section_id');	
//echo(LEPTON_tools::display($page_id, 'pre','ui red message'));
LEPTON_handle::include_files ( [
    "/modules/foldergallery/backend.functions.php",
    "/modules/foldergallery/classes/foldergallery_files.php"
]);

foldergallery_files::repairPositions();

$oFG = foldergallery::getInstance();
$oFG->init_section( $page_id, $section_id );

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('foldergallery');

if(isset($_POST['toggle']) && is_numeric($_POST['toggle']))
{
    $oFG->toggle_active($_POST['toggle']);
}
if(isset($_GET['move_down']) || isset($_GET['move_up']))
{
    $oFG->move();
    // reinit section
    $oFG->init_section( $page_id, $section_id );
}

//  Build categories-tree
$aAllCartegories = [];
$oFG->buildCatTree( 0, $aAllCartegories);

// Add "root" to the top of the tree
$oFG->fg_category_all[0]['subcategories'] = $aAllCartegories;
$aAllCartegories = array(
    $oFG->fg_category_all[0]
);

$data = array(
    'oFG' => $oFG,
    'page_id' => $page_id,
    'section_id' => $section_id,
    'AllCartegories' => $aAllCartegories,
    'leptoken' => get_leptoken()
);

echo $oTWIG->render(
    "@foldergallery/modify.lte", //	template-filename
    $data       //	template-data
);
