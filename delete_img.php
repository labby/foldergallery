<?php

/**
 *  @module         Foldergallery
 *  @version        see info.php of this module
 *  @author         cms-lab (initiated by Jürg Rast)
 *  @copyright      2010-2025 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$oFG = foldergallery::getInstance();

$cat_id = LEPTON_core::getValue('cat_id', 'integer'); // intval($_POST['cat_id']);

if (!is_null($cat_id))
{	
	
	$page_id    = LEPTON_core::getValue('page_id',    'integer'); // intval($_POST['page_id']);
	$section_id = LEPTON_core::getValue('section_id', 'integer'); // intval($_POST['section_id']);
// 	$cat_id     = LEPTON_core::getValue('cat_id',     'integer'); // intval($_POST['cat_id']);
	$iImageID   = LEPTON_core::getValue('delete',     'integer'); // intval($_POST['delete']);
	
	$oFG->init_section($page_id, $section_id);
	$root_dir   = $oFG->fg_settings['root_dir'];
	 
    $display_url = LEPTON_URL.'/modules/foldergallery/modify_cat.php?page_id='.$page_id.'&section_id='.$section_id.'&cat_id='.$cat_id;

	$result = [];	
	LEPTON_database::getInstance()->execute_query(
        "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `id`=". $iImageID,
        true,
        $result,
        false
    );		
	
	if (!empty($result))
	{	
		// delete images
		$bildfilename = $result['file_name'];
		$parent_id = $result['parent_id'];

		$categorie = array();	
		LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_foldergallery_categories` WHERE `id`=".$parent_id,
            true,
            $categorie,
            false
        );		

        if ($categorie['categorie'] == "Root")
        {
            $categorie['categorie'] = "";
        }
        
		$folder = $root_dir."/".$categorie['categorie'];
		
        // 2.1
		$pathToFile = foldergallery::FG_PATH.$folder.'/'.$bildfilename;
		LEPTON_handle::delete_obsolete_files($pathToFile);

        // 2.2
		$pathToThumb = foldergallery::FG_PATH.$folder.foldergallery::FG_THUMBDIR.'/'.$bildfilename;
		LEPTON_handle::delete_obsolete_files($pathToThumb);
		
        // 2.3
		$pathToCropInfo = __DIR__."/custom_crop_storage/".$result['id'].".json";
		LEPTON_handle::delete_obsolete_files($pathToCropInfo);
		
		// 2.4 delete the db entry
		LEPTON_database::getInstance()->simple_query("DELETE FROM `".TABLE_PREFIX."mod_foldergallery_files` WHERE `id`=".$iImageID );

        // 2.5
		LEPTON_admin::getInstance()->print_success($TEXT['SUCCESS'],$display_url);
		
	} 
	else 
	{
		LEPTON_admin::getInstance()->print_error($oFG->language['ERROR_FILE_DELETE']." [1]",$display_url);
	}
} 
else 
{
	LEPTON_admin::getInstance()->print_error($oFG->language['ERROR_FILE_DELETE']." [2]", ADMIN_URL."/login/index.php");
}

LEPTON_admin::getInstance()->print_footer();
